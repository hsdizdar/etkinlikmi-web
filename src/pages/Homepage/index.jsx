// Packages
import React from 'react';
import { Redirect } from 'react-router-dom';

//Components
import HeaderPhoto from '../../components/HeaderPhoto/index';

//Objects
import Header from '../../objects/Header/index';
import Menu from '../../objects/Menu/index';
import HeaderSlider from '../../objects/HeaderSlider/index';
import HomepageContent from "../../objects/HomepageContent/index";
import FooterSlider from '../../objects/FooterSlider/index';
import Footer from '../../objects/Footer/index';

// Local Modules
import './index.css';


export default class Homepage extends React.Component {
  render() {
    return(
      <div className="homepage">
        <Header></Header>
        <HeaderPhoto></HeaderPhoto>
        <Menu></Menu>
        <HeaderSlider></HeaderSlider>
        <HomepageContent></HomepageContent>
        <FooterSlider></FooterSlider>
        <Footer></Footer>
      </div>  
    );
  }
}
