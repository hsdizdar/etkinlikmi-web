// Packages
import React from 'react';
import { Redirect } from 'react-router-dom';
import Slider from 'react-slick';

//Components
import HeaderPhoto from '../../components/HeaderPhoto/index';

// Objects
import Header from '../../objects/Header/index';
import Menu from '../../objects/Menu/index';
import FooterSlider from '../../objects/FooterSlider/index';
import Footer from '../../objects/Footer/index';

//Local Modules
import './index.css';

export default class Detail extends React.Component {
  render() {
    var settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    return(
      <div className="detail-page">
        <Header></Header>
        <HeaderPhoto></HeaderPhoto>
        <Menu></Menu>

        <div className="detail-content">
          <div className="detail-name">
            <div className="detail-name__left">
              <p className="name-bottom">İSİM</p>
              <p className="name-top">İSİM</p>
            </div>
            <div className="detail-name__middle">
              <p className="activity-name">TOLGASHOW</p>
              <p className="activity-type">KOMEDİ</p>
            </div>
            <div className="detail-name__right">
              <p className="date-bottom">21 AĞUSTOS</p>
              <p className="date-top">21 AĞUSTOS</p>
            </div>
          </div>

          <div className="detail-slider">
            <Slider {...settings}>
              <div className="slider-item">
                <img src="/images/tolgashow-detail.jpg"/>
              </div>
              <div className="slider-item">
                <img src="/images/tolgashow-detail-2.jpg"/>
              </div>
            </Slider>
          </div>

          <div className="activity-description">
            <p>What is your attitude as a small town businessman when it comes to advertising or taking help of an advertising design agency to provide creative design solutions? I bet, more often than not it is on the lines of “What ever is left over, we’ll use for advertising”. Well, you are not alone. Most of the small town businessmen treat.</p>
            <a href="http://www.biletix.com">BİLET AL</a>
          </div>

          <div className="detail-city">
            <div className="detail-city__left">
              <p className="city-bottom">ŞEHİR</p>
              <p className="city-top">ŞEHİR</p>
            </div>
            <div className="detail-city__middle">
              <p className="activity-city">TRABZON</p>
            </div>
            <div className="detail-city__right">
              <p className="time-bottom">21:45</p>
              <p className="time-top">21:45</p>
            </div>
          </div>

          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d752.8097480962901!2d39.77052569729367!3d40.99814196561044!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x569d1edb5add3f22!2zQXRhdMO8cmsgS8O8bHTDvHIgTWVya2V6!5e0!3m2!1str!2str!4v1514205121708" width="100%" height="450" frameborder="0" allowfullscreen></iframe>

          <div className="detail-address">
            <p className="address-top">ADRES</p>
            <p className="address-bottom"> KTÜ ATATÜRK KÜLTÜR MERKEZİ</p>
          </div>

          <FooterSlider></FooterSlider>
        </div>
        <Footer></Footer>
      </div>
    );
  }
}