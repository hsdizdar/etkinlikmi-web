// Packages
import React from 'react';
import { Redirect } from 'react-router-dom';

// Objects
import Header from '../../objects/Header/index';
import ActivityForm from '../../objects/ActivityForm/index';
import Footer from '../../objects/Footer/index';

// Local Modules
import './index.css';

export default class AddActivity extends React.Component {
  render() {
    return(
      <div className="add-activity">
        <Header></Header>
        <div className="activity-info">
          <h1 className="activity-title">AKTİVİTE BİLGİSİ</h1>
          <ActivityForm></ActivityForm>
        </div>
        <Footer></Footer>
      </div>
    );
  }
}