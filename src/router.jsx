// Packages
import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom'

// Pages
import Homepage from './pages/Homepage/index';
import Detail from './pages/Detail/index';
import AddActivity from './pages/AddActivity/index';

// Local Modules
import './index.css';

const Generic404 = () => (
  <div>
    <h2>Etkimlik.mi</h2>
    <h3>404 Not Found!</h3>
  </div>
)


const AppRouter = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={Homepage}/>
      <Route exact path="/detail/" component={Detail}/>
      <Route exact path="/activities/" component={AddActivity}/>
      <Route exact path='*' component={Generic404} />
    </Switch>
  </Router>
)


export default AppRouter;
