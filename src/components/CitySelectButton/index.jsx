// Packages
import React from 'react';

// Local Modules
import './index.css';

export default class CitySelectButton extends React.Component {
  render() {
    return(
      <div className ="select-box-city">
        <img src="/images/location-city-material.png"/>
        <select className="city-select">
          <option hidden>Şehir</option>
          <option>İstanbul</option>
          <option>Ankara</option>
          <option>Samsun</option>
          <option>Giresun</option>
        </select>
      </div>
    );
  }
}