// Packages
import React from 'react';

// Local Modules
import './index.css';

export default class DateSelectButton extends React.Component {
  render() {
    return(
      <input type="date" placeholder="Date" className="select-date"/>
    );
  }
}