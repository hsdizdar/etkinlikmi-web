// Packages
import React from 'react';

// Local Modules
import './index.css';

export default class TypeSelectButton extends React.Component {
  render() {
    return(
      <div className="select-box-type">
        <img src="/images/merge-type-material.png"/>
        <select className="type-select">
          <option hidden>Tür</option>
          <option>Tiyatro</option>
          <option>Sinema</option>
          <option>Söyleşi</option>
          <option>Konser</option>
        </select>
      </div>
    );
  }
}