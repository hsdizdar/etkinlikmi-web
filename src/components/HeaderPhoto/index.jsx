// Packages
import React from 'react';
import { Redirect } from 'react-router-dom';

// Local Modules
import './index.css';


export default class Homepage extends React.Component {
  render() {
    return(
      <div className="header-photo">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <img src="/images/header-photo.png" alt="wrapper-photo"/>
              <h1 className="header-text">Etkinlik Bul</h1>
            </div>
          </div>
        </div>
      </div>
    );
  }
}