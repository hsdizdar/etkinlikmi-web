// Packages
import React from 'react';

// Local Modules
import './index.css';


export default class DetailButton extends React.Component {
  render() {
    return(
      <a href="/detail/" className="detail-button">Detay</a> 
    );
  }
}
