// Packages
import React from 'react';
import Modal from 'react-modal';
import SimpleLineIcon from 'react-simple-line-icons';

// Local Modules
import './index.css';

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

export default class LoginForm extends React.Component {
  constructor() {
    super();

    this.state = {
      modalIsOpen: false
    };

    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal() {
    this.setState({modalIsOpen: true});
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    this.subtitle.style.color = '#ff5151';
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  render() {
    return(
      <div className="login">
        <button className="login-open" onClick={this.openModal}>Giriş</button>
        <Modal
        isOpen={this.state.modalIsOpen}
        onAfterOpen={this.afterOpenModal}
        onRequestClose={this.closeModal}
        style={customStyles}
        >
          <h1 className="log-title" ref={subtitle => this.subtitle = subtitle}>ÜYE GİRİŞİ</h1>
          <button className="login-close" onClick={this.closeModal}>X</button>
          <form>
            <input 
              type="email" id="id_email"
              className="email" name="email"
              placeholder="E-Mail"/>
            <input 
              type="password" id="password"
              className="password" name="password"
              placeholder="Şifre"/>
            <div className="log-icon">
              <SimpleLineIcon name="eye"/>
            </div>
            <button className="login-entry">Giriş</button>
          </form>
          <p className="login-text">Henüz bir hesaba sahip değilseniz. Üye olunuz.</p>
        </Modal>
      </div>
        
    );
  }
}