// Packages
import React from 'react';

//Objects
import CitySelectButton from '../../components/CitySelectButton/index';
import TypeSelectButton from '../../components/TypeSelectButton/index';
import DateSelectButton from '../../components/DateSelectButton/index';

// Local Modules
import './index.css';


export default class Homepage extends React.Component {
  render() {
    return(
      <div className="menu">
        <div className="container">
          <div className="row">
            <div className="col-xs-4">
              <CitySelectButton></CitySelectButton>
            </div>
            <div className="col-xs-4">
              <TypeSelectButton></TypeSelectButton>
            </div>
            <div className="col-xs-4">
              <DateSelectButton></DateSelectButton>
            </div>
          </div>
        </div>
      </div> 
    );
  }
}