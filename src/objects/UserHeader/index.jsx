// Packages
import React from 'react';
import SimpleLineIcon from 'react-simple-line-icons';

// Local Modules
import './index.css';


export default class Header extends React.Component {
  render() {
    return(
      <div className="user-header">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <div className="user-header-right">
                <div className="search">
                  <input type="text" name="search" placeholder="Search"/>
                  <SimpleLineIcon name="magnifier"/>
                </div>
                <a className="aktivities-button" href="/activities/">Aktivite Ekle</a>
                <div className="exit">
                  <a href="/">
                    <SimpleLineIcon name="logout"/>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>    
      </div>
    );
  }
}
