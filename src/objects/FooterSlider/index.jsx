// Packages
import React from 'react';
import Slider from 'react-slick';

// Local Modules
import './index.css';


export default class FooterSlider extends React.Component {
  render() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    return(
      <div className="footer-slider">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <div className="title">
                <p>Sizin Seçtikleriniz...</p>
                <div className="line"></div>
              </div> 
              <Slider {...settings}>
                <div className="choices">
                  <div className="container">
                    <div className="row">
                      <div className="col-xs-3">
                        <img src="/images/kardesiminhikayesi.jpg"/>
                      </div>
                      <div className="col-xs-3">
                        <img src="/images/lordofwar.jpg"/>
                      </div>
                      <div className="col-xs-3">
                        <img src="/images/ikincisans.jpg"/>
                      </div>
                      <div className="col-xs-3">
                        <img src="/images/sekerportakalı.jpg"/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="choices">
                  <div className="container">
                    <div className="row">
                      <div className="col-xs-3">
                        <img src="/images/kardesiminhikayesi.jpg"/>
                      </div>
                      <div className="col-xs-3">
                        <img src="/images/lordofwar.jpg"/>
                      </div>
                      <div className="col-xs-3">
                        <img src="/images/ikincisans.jpg"/>
                      </div>
                      <div className="col-xs-3">
                        <img src="/images/sekerportakalı.jpg"/>
                      </div>
                    </div>
                  </div>
                </div>
              </Slider>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
