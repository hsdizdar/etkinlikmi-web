// Packages
import React from 'react';

// Local Modules
import TypeSelectButton from '../../components/TypeSelectButton/index';
import CitySelectButton from '../../components/CitySelectButton/index';
import DateSelectButton from '../../components/DateSelectButton/index';
import './index.css';

export default class ActivityForm extends React.Component {
  render() {
    return(
      <form>
        <div className="info-form">
          <div className="container">
            <div className="row">
              <div className="col-xs-12">
                <input 
                  type="text" id="id_activity_name"
                  className="activity_name" name="activity_name"
                  placeholder="Aktivite İsmi"/>
              </div>
              <div className="col-xs-12">
                <TypeSelectButton></TypeSelectButton>
              </div>
              <div className="col-xs-12">
                <CitySelectButton></CitySelectButton>
              </div>
              <div className="col-xs-12">
                <DateSelectButton></DateSelectButton> 
              </div>
              <div className="col-xs-6">
                <input 
                  type="text" id="id_start_time"
                  className="start_time" name="start_time"
                  placeholder="Başlangıç Saati"/>
              </div>
              <div className="col-xs-6">
                <input 
                  type="text" id="id_finish_time"
                  className="finish_time" name="finish_time"
                  placeholder="Bitiş Saati"/>
              </div>
              <div className="col-xs-12">
                <textarea 
                  type="text" id="id_activity_adress"
                  className="activity_adress" name="activity_adress"
                  placeholder="Adres">
                </textarea>
              </div>
              <div className="col-xs-12">
                <textarea 
                  type="text" id="id_activity_description"
                  className="activity_description" name="activity_description"
                  placeholder="Açıklama">
                </textarea>
              </div>
              <div className="col-xs-12">
                <div className="upload-wrapper">
                  <button className="upload-button">Resim Seç</button>
                  <input 
                    type="file" id="id_file" 
                    className="file" name="file"/>
                </div>
                <p><span>*</span> 940*380 boyutunda resimler yükleyiniz.</p>
              </div>
              <div className="col-xs-12">
                <button className="activity-button">AKTİVİTEYİ PAYLAŞ</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    );
  }
}