// Packages
import React from 'react';
import SimpleLineIcon from 'react-simple-line-icons';

// Local Modules
import LoginForm from '../../objects/LoginForm/index';
import RegisterForm from '../../objects/RegisterForm/index';
import './index.css';


export default class Header extends React.Component {
  render() {
    return(
      <div className="header">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <div className="header-right">
                <div className="search">
                  <input type="text" name="search" placeholder="Search"/>
                  <SimpleLineIcon name="magnifier"/>
                </div>
                <LoginForm></LoginForm>
                <RegisterForm></RegisterForm>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
