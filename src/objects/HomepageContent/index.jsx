// Packages
import React from 'react';

// Local Modules
import DetailButton from '../../components/DetailButton/index';
import './index.css';


export default class HomepageContent extends React.Component {
  render() {
    return(
      <div className="content">
        <div className="content-top">
          <div className="container">
            <div className="row">
              <div className="col-md-4 col-sm-6 col-xs-12">
                <a href="#">
                  <div className="content-detail">
                    <img className="content-item" src="/images/cemadrian-concert.jpg"/>
                    <div className="content-detail-text">
                      <p className="event-type">KONSER</p>
                      <p className="event-own">Cem Adrian</p>
                      <DetailButton></DetailButton>
                    </div>
                  </div>
                </a>
              </div>
              <div className="col-md-4 col-sm-6 col-xs-12">
                <a href="#">
                  <div className="content-detail">
                    <img className="content-item" src="/images/zulfulivaneli-sign-day.jpg"/>
                    <div className="content-detail-text">
                      <p className="event-type">İMZA GÜNÜ</p>
                      <p className="event-own">Zülfü Livaneli</p>
                      <DetailButton></DetailButton>
                    </div>
                  </div>
                </a>
              </div>
              <div className="col-md-4 hidden-sm col-xs-12">
                <a href="#">
                  <div className="content-detail">
                    <img className="content-item" src="/images/theater.jpg"/>
                    <div className="content-detail-text">
                      <p className="event-type">TİYATRO</p>
                      <p className="event-own">Doğan Turan</p>
                      <DetailButton></DetailButton>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="content-bottom">
          <div className="container">
            <div className="row">
              <div className="col-md-4 col-sm-6 hidden-xs">
                <a href="#">
                  <div className="content-detail">
                    <img className="content-item" src="/images/tolgashow.jpg"/>
                    <div className="content-detail-text">
                      <p className="event-type">TALKSHOW</p>
                      <p className="event-own">Tolga Çevik</p>
                      <DetailButton></DetailButton>
                    </div>
                  </div>
                </a>
              </div>
              <div className="col-md-4 col-sm-6 hidden-xs">
                <a href="#">
                  <div className="content-detail">
                    <img className="content-item" src="/images/teoman-concert.jpg"/>
                    <div className="content-detail-text">
                      <p className="event-type">KONSER</p>
                      <p className="event-own">Teoman</p>
                      <DetailButton></DetailButton>
                    </div>
                  </div>
                </a>
              </div>
              <div className="col-md-4 hidden-sm hidden-xs">
                <a href="#">
                  <div className="content-detail">
                    <img className="content-item" src="/images/erdilyasaroglu-conversation.jpg"/>
                    <div className="content-detail-text">
                      <p className="event-type">SÖYLEŞİ</p>
                      <p className="event-own">Erdil Yaşaroğlu</p>
                      <DetailButton></DetailButton>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}