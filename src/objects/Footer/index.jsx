// Packages
import React from 'react';

// Local Modules
import './index.css';


export default class Footer extends React.Component {
  render() {
    return(
      <div className="footer">    
        <div className="container-fluid">
          <div className="row">
            <div className="col-xs-12">
              <p>© 2017 Etkinlik.com. Tüm hakları saklıdır.</p>
              <p>Bu web sitesinin kullanımı, ticari kullanımı engelleyen Kullanım Koşulları'na tabidir. Bu sayfayı geçtiğinizde bu koşulları kabul etmiş sayılırsınız.</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}