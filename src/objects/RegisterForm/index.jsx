// Packages
import React from 'react';
import Modal from 'react-modal';
import SimpleLineIcon from 'react-simple-line-icons';

// Local Modules
import './index.css';

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

export default class RegisterForm extends React.Component {
  constructor() {
    super();

    this.state = {
      modalIsOpen: false
    };

    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal() {
    this.setState({modalIsOpen: true});
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    this.subtitle.style.color = '#ff5151';
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  render() {
    return(
      <div className="register">
        <button className="register-open" onClick={this.openModal}>Üyelik</button>
        <Modal
        isOpen={this.state.modalIsOpen}
        onAfterOpen={this.afterOpenModal}
        onRequestClose={this.closeModal}
        style={customStyles}
        >
          <h1 className="reg-title" ref={subtitle => this.subtitle = subtitle}>ÜYE OL</h1>
          <button className="register-close" onClick={this.closeModal}>X</button>
          <form>
            <input 
              type="email" id="id_email"
              className="email" name="email"
              placeholder="E-Mail"/>
            <input 
              type="text" id="id_username"
              className="username" name="username"
              placeholder="Kullanıcı Adı"/>
            <input 
              type="password" id="password"
              className="password" name="password"
              placeholder="Şifre"/>
            <div className="reg-icon">
              <SimpleLineIcon name="eye"/>
            </div>
            <div className="select-box">
              <select>
                <option hidden>Şehir</option>
                <option>Ankara</option>
                <option>Giresun</option>
                <option>Samsun</option>
                <option>İstanbul</option>
              </select>
            </div>
            <button className="register-entry">Üye Ol</button>
          </form>
          <p className="register-text">Bir hesaba sahipseniz. Üye girişi yapınız.</p>
        </Modal>
      </div>
        
    );
  }
}