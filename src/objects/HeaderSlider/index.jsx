// Packages
import React from 'react';
import Slider from 'react-slick';

// Local Modules
import DetailButton from "../../components/DetailButton/index";
import './index.css';


export default class HeaderSlider extends React.Component {
  render() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    return(
      <div className="header-slider">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <Slider {...settings}>
                <div className="event">
                  <img src="/images/box-match.jpg"/>
                  <p className="event-name">Rico VS Badr - Boks Maçı</p>
                  <p className="event-date">21 Mart 2018</p>
                  <DetailButton></DetailButton>
                </div>
                <div className="event">
                  <img src="/images/box-match.jpg"/>
                  <p className="event-name">Rico VS Badr - Boks Maçı</p>
                  <p className="event-date">21 Mart 2018</p>
                  <DetailButton></DetailButton>
                </div>
                <div className="event">
                  <img src="/images/box-match.jpg"/>
                  <p className="event-name">Rico VS Badr - Boks Maçı</p>
                  <p className="event-date">21 Mart 2018</p>
                  <DetailButton></DetailButton>
                </div>
                <div className="event">
                  <img src="/images/box-match.jpg"/>
                  <p className="event-name">Rico VS Badr - Boks Maçı</p>
                  <p className="event-date">21 Mart 2018</p>
                  <DetailButton></DetailButton>
                </div>
              </Slider>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
